from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired
from flask_ckeditor import CKEditorField


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired()])
    password = StringField('Hasło', validators=[DataRequired()])
    submit = SubmitField('Zaloguj')

class RegisterForm(FlaskForm):
    name = StringField('Nazwa', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired()])
    password = StringField('Hasło', validators=[DataRequired()])
    submit = SubmitField('Zaloguj')

class CreatePost(FlaskForm):
    title = StringField('Co mnie 😡😡😡')
    submit = SubmitField('Zapisz')
