from datetime import date

from flask import Flask, abort, render_template, redirect, url_for, flash
from flask_bootstrap import Bootstrap5
from werkzeug.security import generate_password_hash, check_password_hash
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin, login_user, LoginManager, current_user, logout_user, login_required
from sqlalchemy.orm import relationship

import forms

# FLASK:
app = Flask(__name__)
app.config['SECRET_KEY'] = 'topsecret'
Bootstrap5(app)

# SQL:
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///wypo.db'
db = SQLAlchemy()
db.init_app(app)

# LOGIN:
login_manager = LoginManager()
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(100), unique=True)
    post_id = relationship('Posts', back_populates='by_user_id')


class Posts(db.Model):
    __tablename__ = 'posts'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(600))
    stars = db.Column(db.Integer)
    date = db.Column(db.String(250), nullable=False)
    by_user_id = relationship('User', back_populates='post_id')
    author_id = db.Column(db.Integer, db.ForeignKey('users.id'))


with app.app_context():
    db.create_all()


def check_User():
    print(current_user)
    if current_user.is_authenticated:

        return current_user.name
    else:
        return 'Użytkownik Anonimowy'


@app.route('/')
def home():
    get_posts = db.session.execute(db.Select(Posts))
    posts = get_posts.scalars().all()
    return render_template('index.html', posts=posts)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if not current_user.is_anonymous:
        return redirect(url_for('home'))

    login_form = forms.LoginForm()

    if login_form.validate_on_submit():
        check_user = User.query.filter_by(email=login_form.email.data).first()
        if check_user and check_password_hash(check_user.password, login_form.password.data):
            login_user(check_user)
            return redirect(url_for('home'))
        else:
            flash('Please check your login details and try again.')
            return redirect(url_for('login'))

    return render_template('login.html', form=login_form)


@app.route('/register', methods=['GET', 'POST'])
def register():
    if not current_user.is_anonymous:
        return redirect(url_for('home'))

    register_form = forms.RegisterForm()

    if register_form.validate_on_submit():

        check_user = User.query.filter_by(email=register_form.email.data).first()
        if check_user:
            flash('Użytkownik o podanym mailu istnieje.')
            return redirect(url_for('register'))

        new_user = User(
            email=register_form.email.data,
            password=generate_password_hash(password=register_form.password.data, salt_length=6),
            name=register_form.name.data
        )
        db.session.add(new_user)
        db.session.commit()

        login_user(new_user)
        return redirect(url_for('home'))

    return render_template('register.html', form=register_form)


@app.route('/create_post', methods=['GET','POST'])
def create_post():
    post_form = forms.CreatePost()

    if post_form.validate_on_submit():
        new_post = Posts(
            title=post_form.title.data,
            stars=0,
            author_id=check_User(),
            date=date.today().strftime("%B %d, %Y")
        )
        db.session.add(new_post)
        db.session.commit()

        return redirect(url_for('home'))

    return render_template('create_post.html', form=post_form)


@app.route('/logout')
@login_required
def logout():
    if current_user.is_authenticated:
        print(f"Goodbye!: {current_user.name}")
        logout_user()  # LOGOUT USER, CLEAN UP!
    return redirect(url_for('home'))


@app.route('/delete/<int:post_id>')
def delete(post_id):
    delete_post = Posts.query.filter_by(id=post_id).first()
    db.session.delete(delete_post)
    db.session.commit()
    return redirect(url_for('home'))


if __name__ == '__main__':
    app.run(debug=True, port=8000)
